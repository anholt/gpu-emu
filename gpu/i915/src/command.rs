use crate::DeviceBuf;
use anyhow::{bail, Context, Result};
use num_traits::FromPrimitive;

// Note: 3DSTATE_* commands in the spec are named Gpu here, to avoid starting
// identifiers with a number.

fn take_bits(val: &mut u32, low: u32, high: u32) -> u32 {
    assert!(low <= high);
    assert!(high < 32);

    let result = (*val >> low) & ((1u32 << (high - low + 1)) - 1);
    *val &= !(result << low);

    result
}

fn take_fixed_len(val: &mut u32, len: u32) -> Result<()> {
    let cmd_len = take_bits(val, 0, 15);
    if cmd_len != len {
        bail!(
            "Command encoded incorrect fixed length field ({}, should be {})",
            cmd_len,
            len
        );
    }
    Ok(())
}

fn take_bool(val: &mut u32, bit: u32) -> bool {
    let bit = take_bits(val, bit, bit);

    bit != 0
}

pub trait BatchCommand {
    fn encode(&self, dwords: &mut [u32]);

    fn len(&self) -> u32;
}

#[derive(Debug)]
pub struct MiNoop {}

impl BatchCommand for MiNoop {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = 0;
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug)]
pub struct MiFlush {
    pub end_scene: bool,
    pub scene_count: bool,
    pub render_cache_flush_inhibit: bool,
    pub map_cache_invalidate: bool,
}

impl BatchCommand for MiFlush {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = (0x04 << 23)
            | (self.end_scene as u32) << 4
            | (self.scene_count as u32) << 3
            | (self.render_cache_flush_inhibit as u32) << 2
            | (self.map_cache_invalidate as u32);
    }

    fn len(&self) -> u32 {
        1
    }
}

fn mi_parse(_buf: &DeviceBuf, _offset: u32, val: u32) -> Result<Command> {
    let mut val = val;

    let c = match take_bits(&mut val, 23, 28) {
        0x00 => {
            if val != 0 {
                bail!("Unknown MI_NOOP bits: {:#b}\n", val);
            }
            Command::Noop(MiNoop {})
        }
        0x04 => {
            let end_scene = take_bool(&mut val, 4);
            let scene_count = take_bool(&mut val, 3);
            let render_cache_flush_inhibit = take_bool(&mut val, 2);
            let map_cache_invalidate = take_bool(&mut val, 0);
            if val != 0 {
                bail!("Unknown MI_FLUSH bits: {:#b}\n", val);
            }

            Command::Flush(MiFlush {
                end_scene,
                scene_count,
                render_cache_flush_inhibit,
                map_cache_invalidate,
            })
        }
        x => bail!("Unknown MI instruction opcode {}", x),
    };

    Ok(c)
}

fn gpu_24_p_header(cmd: u32) -> u32 {
    assert_eq!(cmd & 31, cmd);
    (0x3 << 29) | (cmd << 24)
}

fn gpu_24_np_header(cmd: u32) -> u32 {
    assert_eq!(cmd & 31, cmd);
    (0x3 << 29) | (cmd << 24)
}

fn gpu_mw_p_header(cmd: u32) -> u32 {
    assert_eq!(cmd & 0xff, cmd);
    (0x3 << 29) | (0x1d << 24) | (cmd << 16)
}

fn gpu_mw_np_header(cmd: u32) -> u32 {
    assert_eq!(cmd & 0xff, cmd);
    (0x3 << 29) | (0x1d << 24) | (cmd << 16)
}

fn gpu_16_np_header(cmd: u32) -> u32 {
    assert_eq!(cmd & 31, cmd);
    (0x3 << 29) | (0x1c << 24) | (cmd << 19)
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuLineAaWidth {
    Pix0_5 = 0x0,
    Pix1_0 = 0x1,
    Pix2_0 = 0x2,
    Pix4_0 = 0x3,
}

#[derive(Debug)]
pub struct GpuAntiAliasing {
    pub line_cap_aa_width: Option<GpuLineAaWidth>,
    pub line_aa_width: Option<GpuLineAaWidth>,
}

impl BatchCommand for GpuAntiAliasing {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_24_p_header(0x06);
        if let Some(aa) = self.line_cap_aa_width {
            dwords[0] |= 1 << 16;
            dwords[0] |= (aa as u32) << 14;
        }
        if let Some(aa) = self.line_aa_width {
            dwords[0] |= 1 << 8;
            dwords[0] |= (aa as u32) << 6;
        }
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuPrimType {
    TriList = 0x00,
    TriStrip = 0x01,
    TriStripReverse = 0x02,
    TriFan = 0x03,
    Polygon = 0x04,
    LineList = 0x05,
    LineStrip = 0x06,
    RectList = 0x07,
    PointList = 0x08,
    DIB = 0x09,
    ClearRect = 0x0a,
    ZoneInit = 0x0d,
}

#[derive(Debug)]
pub struct GpuPrimitive {
    pub vertex_indirect: bool,
    pub access_random: bool,
    pub prim_type: GpuPrimType,
    pub len: u32,
}

impl BatchCommand for GpuPrimitive {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_24_p_header(0x1f)
            | (self.vertex_indirect as u32) << 23
            | (self.prim_type as u32) << 18;
        if self.vertex_indirect {
            dwords[0] |= (self.access_random as u32) << 17;
            assert!(self.len < 1 << 16);
        } else {
            assert!(self.len < 1 << 18);
        }
        dwords[0] |= self.len;
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuBufferID {
    Color = 0x03,
    ColorAux = 0x04,
    ColorMcIntraCorr = 0x05,
    Depth = 0x07,
}

#[derive(Debug)]
pub struct GpuBufferInfo {
    pub aux_buffer_id: bool,
    pub buffer_id: GpuBufferID,

    pub use_fence: bool,
    pub tiled: bool,
    pub tiled_y: bool,

    pub pitch: u32,  // in bytes, must be a multiple of 4.
    pub offset: u32, // GPU vaddr, must be a multiple of 4
}

impl BatchCommand for GpuBufferInfo {
    fn encode(&self, dwords: &mut [u32]) {
        // TODO: More packing
        dwords[0] = gpu_mw_np_header(0x8e) | 1;

        dwords[1] = (self.buffer_id as u32) << 24;
        assert!(self.pitch.trailing_zeros() >= 2);
        dwords[1] |= self.pitch;

        assert!(self.offset.trailing_zeros() >= 2);
        dwords[2] = self.offset;
    }

    fn len(&self) -> u32 {
        3
    }
}

#[derive(Debug)]
pub struct GpuConstantBlendColor {
    pub rgba: (u8, u8, u8, u8),
}

impl BatchCommand for GpuConstantBlendColor {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x88);
        dwords[1] = encode_argb(self.rgba);
    }

    fn len(&self) -> u32 {
        2
    }
}

#[derive(Copy, Clone, Debug)]
pub struct GpuCoordSetBindings {
    pub src: [u8; 8],
}

impl BatchCommand for GpuCoordSetBindings {
    fn encode(&self, dwords: &mut [u32]) {
        for src in &self.src {
            assert!(src & 7 == *src);
        }

        dwords[0] = gpu_24_np_header(0x16)
            | (self.src[0] as u32)
            | (self.src[1] as u32) << 3
            | (self.src[2] as u32) << 6
            | (self.src[3] as u32) << 9
            | (self.src[4] as u32) << 12
            | (self.src[5] as u32) << 15
            | (self.src[6] as u32) << 18
            | (self.src[7] as u32) << 21;
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug)]
pub struct GpuDefaultDiffuse {
    pub rgba: (u8, u8, u8, u8),
}

fn encode_argb(rgba: (u8, u8, u8, u8)) -> u32 {
    (rgba.3 as u32) << 24 | (rgba.0 as u32) << 16 | (rgba.1 as u32) << 8 | rgba.2 as u32
}

impl BatchCommand for GpuDefaultDiffuse {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x99);
        dwords[1] = encode_argb(self.rgba);
    }

    fn len(&self) -> u32 {
        2
    }
}

#[derive(Debug)]
pub struct GpuDefaultSpecular {
    pub rgba: (u8, u8, u8, u8),
}

impl BatchCommand for GpuDefaultSpecular {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x9a);
        dwords[1] = encode_argb(self.rgba);
    }

    fn len(&self) -> u32 {
        2
    }
}

#[derive(Debug)]
pub struct GpuDefaultZ {
    pub z: f32,
}

impl BatchCommand for GpuDefaultZ {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x98);
        dwords[1] = self.z.to_bits();
    }

    fn len(&self) -> u32 {
        2
    }
}

#[derive(Debug)]
pub struct GpuDepthOffsetScale {
    pub scale: f32,
}

impl BatchCommand for GpuDepthOffsetScale {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x97);
        dwords[1] = self.scale.to_bits();
    }

    fn len(&self) -> u32 {
        2
    }
}

#[derive(Debug)]
pub struct GpuDepthSubrectangleEnable {
    pub modify_enable: bool,
    pub enable: bool,
}

impl BatchCommand for GpuDepthSubrectangleEnable {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] =
            gpu_16_np_header(0x11) | ((self.modify_enable as u32) << 1) | (self.enable as u32);
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug)]
pub struct GpuDrawingRectangle {
    pub clip_xmin: u16,
    pub clip_ymin: u16,
    pub clip_xmax: u16,
    pub clip_ymax: u16,
    pub draw_x: i16,
    pub draw_y: i16,
}

impl BatchCommand for GpuDrawingRectangle {
    fn encode(&self, dwords: &mut [u32]) {
        // TODO: More packing
        dwords[0] = gpu_mw_np_header(0x80) | 3;
        dwords[1] = 0;
        dwords[2] = self.clip_xmin as u32 | ((self.clip_ymin as u32) << 16);
        dwords[3] = self.clip_xmax as u32 | ((self.clip_ymax as u32) << 16);
        dwords[4] = self.draw_x as u32 | ((self.draw_y as u32) << 16);
    }

    fn len(&self) -> u32 {
        5
    }
}

#[derive(Debug)]
pub struct GpuLoadIndirectState {}

impl BatchCommand for GpuLoadIndirectState {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_p_header(0x07);
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S0 {
    pub vertex_buffer_address: u32,
    pub vertex_cache_invalidate_disable: bool,
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S1 {
    /// Vertex buffer width (used values) in dwords.  Valid values 2..41.
    pub vb_width: u32,
    /// Vertex buffer pitch in dwords.  Valid values 2..63.
    pub vb_pitch: u32,
    /// When > 0, specifies the maximum valid index in the VB.
    pub vb_max_index: u16,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum TCFormat {
    TexCoordFormat2FP = 0x00,
    TexCoordFormat3FP = 0x01,
    TexCoordFormat4FP = 0x02,
    TexCoordFormat1FP = 0x03,
    TexCoordFormat2FP16 = 0x04,
    TexCoordFormat4FP16 = 0x05,
    TexCoordFormatNotPresent = 0x0f,
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S2 {
    tc_formats: [TCFormat; 8],
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S3 {
    tc_wrap_shortest_x: [bool; 8],
    tc_wrap_shortest_y: [bool; 8],
    tc_wrap_shortest_z: [bool; 8],
    tc_perspective_correction: [bool; 8],
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum CullMode {
    Both = 0x00,
    None = 0x01,
    CW = 0x02,
    CCW = 0x03,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum PositionMask {
    XYZ = 0x00,
    XYZW = 0x01,
    XY = 0x02,
    XYW = 0x03,
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S4 {
    point_width: u32,
    line_width: f32,
    alpha_shade_flat: bool,
    fog_shade_flat: bool,
    specular_shade_flat: bool,
    color_shade_flat: bool,
    cull_mode: CullMode,
    point_width_per_vertex: bool,
    specular: bool,
    diffuse: bool,
    local_depth_offset_per_vertex: bool,
    position_mask: PositionMask,
    force_default_diffuse: bool,
    force_default_specular: bool,
    local_depth_offset: bool,
    fog: bool,
    point_sprite: bool,
    line_aa: bool,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum CompareFunc {
    Always = 0x00,
    Never = 0x01,
    Less = 0x02,
    Equal = 0x03,
    LessEqual = 0x04,
    Greater = 0x05,
    NotEqual = 0x06,
    GreaterEqual = 0x07,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum StencilOp {
    Keep = 0x00,
    Zero = 0x01,
    Replace = 0x02,
    IncrSat = 0x03,
    DecrSat = 0x04,
    Incr = 0x05,
    Decr = 0x06,
    Invert = 0x07,
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S5 {
    write_a: bool,
    write_r: bool,
    write_g: bool,
    write_b: bool,
    force_default_point_width: bool,
    last_pixel: bool,
    global_depth_offset: bool,
    fog: bool,
    stencil_ref: u8,
    stencil_func: CompareFunc,
    stencil_fail_op: StencilOp,
    stencil_zfail_op: StencilOp,
    stencil_zpass_op: StencilOp,
    stencil_write: bool,
    stencil_test: bool,
    color_dither: bool,
    logic_op: bool,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum BlendFunc {
    Add = 0x00,
    Sub = 0x01,
    ReverseSub = 0x02,
    Min = 0x03,
    Max = 0x04,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum ProvokingVertex {
    V0 = 0x00,
    V1 = 0x01,
    V2 = 0x02,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum BlendFactor {
    Zero = 0x01,
    One = 0x02,
    SrcColor = 0x03,
    InvSrcColor = 0x04,
    SrcAlpha = 0x05,
    InvSrcAlpha = 0x06,
    DstAlpha = 0x07,
    InvDstAlpha = 0x08,
    DstColor = 0x09,
    InvDstColor = 0x0a,
    SrcAlphaSat = 0x0b,
    ConstColor = 0x0c,
    InvConstColor = 0x0d,
    ConstAlpha = 0x0e,
    InvConstAlpha = 0x0f,
}

#[derive(Debug, Copy, Clone)]
pub struct GpuLoadStateImmediate1S6 {
    alpha_test: bool,
    alpha_func: CompareFunc,
    alpha_ref: u8,
    depth_test: bool,
    depth_func: CompareFunc,
    blend: bool,
    blend_func: BlendFunc,
    src_blend: BlendFactor,
    dst_blend: BlendFactor,
    depth_write: bool,
    color_write: bool,
    provoking_vertex: ProvokingVertex,
}

#[derive(Debug)]
pub struct GpuLoadStateImmediate1 {
    pub s0: Option<GpuLoadStateImmediate1S0>,
    pub s1: Option<GpuLoadStateImmediate1S1>,
    pub s2: Option<GpuLoadStateImmediate1S2>,
    pub s3: Option<GpuLoadStateImmediate1S3>,
    pub s4: Option<GpuLoadStateImmediate1S4>,
    pub s5: Option<GpuLoadStateImmediate1S5>,
    pub s6: Option<GpuLoadStateImmediate1S6>,
    pub s7_depth_offset: Option<f32>,
}

impl BatchCommand for GpuLoadStateImmediate1 {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_p_header(0x04);

        let mut next_dw = 1;
        if let Some(s0) = &self.s0 {
            assert!(s0.vertex_buffer_address.trailing_zeros() >= 2);
            dwords[next_dw] =
                s0.vertex_buffer_address | (s0.vertex_cache_invalidate_disable as u32);
            next_dw += 1;
        }

        if let Some(s1) = &self.s1 {
            assert!(s1.vb_width >= 2);
            assert!(s1.vb_width <= 41);
            assert!(s1.vb_pitch >= 2);
            assert!(s1.vb_pitch <= 63);
            dwords[next_dw] = s1.vb_width << 24 | s1.vb_pitch << 16 | (s1.vb_max_index as u32);
            next_dw += 1;
        }

        if let Some(s2) = &self.s2 {
            dwords[next_dw] = 0;
            for i in 0..8 {
                dwords[next_dw] |= (s2.tc_formats[i] as u32) << (4 * i);
            }
            next_dw += 1;
        }

        if let Some(s3) = &self.s3 {
            dwords[next_dw] = 0;
            for i in 0..8 {
                if s3.tc_wrap_shortest_x[i] {
                    dwords[next_dw] |= 1 << (i * 4 + 3);
                }
                if s3.tc_wrap_shortest_y[i] {
                    dwords[next_dw] |= 1 << (i * 4 + 2);
                }
                if s3.tc_wrap_shortest_z[i] {
                    dwords[next_dw] |= 1 << (i * 4 + 1);
                }
                if !s3.tc_perspective_correction[i] {
                    dwords[next_dw] |= 1 << (i * 4);
                }
            }
            next_dw += 1;
        }

        #[allow(clippy::float_cmp)]
        if let Some(s4) = &self.s4 {
            dwords[next_dw] = 0;

            assert!(s4.point_width <= 256 && s4.point_width >= 1);
            dwords[next_dw] |= (s4.point_width) << 23;

            assert!(s4.line_width >= 0.0);
            assert!(s4.line_width <= 7.5);
            let line_width = (s4.line_width * 2.0) as u32;
            assert!(s4.line_width == line_width as f32 / 2.0);
            dwords[next_dw] |= line_width << 19;

            dwords[next_dw] |= (s4.alpha_shade_flat as u32) << 18;
            dwords[next_dw] |= (s4.fog_shade_flat as u32) << 17;
            dwords[next_dw] |= (s4.specular_shade_flat as u32) << 16;
            dwords[next_dw] |= (s4.color_shade_flat as u32) << 15;
            dwords[next_dw] |= (s4.cull_mode as u32) << 13;
            dwords[next_dw] |= (s4.point_width_per_vertex as u32) << 12;
            dwords[next_dw] |= (s4.specular as u32) << 11;
            dwords[next_dw] |= (s4.diffuse as u32) << 10;
            dwords[next_dw] |= (s4.local_depth_offset_per_vertex as u32) << 9;
            dwords[next_dw] |= (s4.position_mask as u32) << 6;
            dwords[next_dw] |= (s4.force_default_diffuse as u32) << 5;
            dwords[next_dw] |= (s4.force_default_specular as u32) << 4;
            dwords[next_dw] |= (s4.local_depth_offset as u32) << 3;
            dwords[next_dw] |= (s4.fog as u32) << 2;
            dwords[next_dw] |= (s4.point_sprite as u32) << 1;
            dwords[next_dw] |= s4.line_aa as u32;

            next_dw += 1;
        }

        if let Some(s5) = &self.s5 {
            dwords[next_dw] = 0;

            dwords[next_dw] |= ((!s5.write_a) as u32) << 31;
            dwords[next_dw] |= ((!s5.write_r) as u32) << 30;
            dwords[next_dw] |= ((!s5.write_g) as u32) << 29;
            dwords[next_dw] |= ((!s5.write_b) as u32) << 28;

            dwords[next_dw] |= (s5.force_default_point_width as u32) << 27;
            dwords[next_dw] |= (s5.last_pixel as u32) << 26;
            dwords[next_dw] |= (s5.global_depth_offset as u32) << 25;
            dwords[next_dw] |= (s5.fog as u32) << 24;
            dwords[next_dw] |= (s5.stencil_ref as u32) << 16;
            dwords[next_dw] |= (s5.stencil_func as u32) << 13;
            dwords[next_dw] |= (s5.stencil_fail_op as u32) << 10;
            dwords[next_dw] |= (s5.stencil_zfail_op as u32) << 7;
            dwords[next_dw] |= (s5.stencil_zpass_op as u32) << 4;
            dwords[next_dw] |= (s5.stencil_write as u32) << 3;
            dwords[next_dw] |= (s5.stencil_test as u32) << 2;
            dwords[next_dw] |= (s5.color_dither as u32) << 1;
            dwords[next_dw] |= s5.logic_op as u32;

            next_dw += 1;
        }

        if let Some(s6) = &self.s6 {
            dwords[next_dw] = 0;

            dwords[next_dw] |= (s6.alpha_test as u32) << 31;
            dwords[next_dw] |= (s6.alpha_func as u32) << 28;
            dwords[next_dw] |= (s6.alpha_ref as u32) << 20;
            dwords[next_dw] |= (s6.depth_test as u32) << 19;
            dwords[next_dw] |= (s6.depth_func as u32) << 16;
            dwords[next_dw] |= (s6.blend as u32) << 15;
            dwords[next_dw] |= (s6.blend_func as u32) << 12;
            dwords[next_dw] |= (s6.src_blend as u32) << 8;
            dwords[next_dw] |= (s6.dst_blend as u32) << 4;
            dwords[next_dw] |= (s6.depth_write as u32) << 3;
            dwords[next_dw] |= (s6.color_write as u32) << 2;
            dwords[next_dw] |= s6.provoking_vertex as u32;

            next_dw += 1;
        }

        if let Some(s7) = &self.s7_depth_offset {
            dwords[next_dw] = s7.to_bits();
        }
    }

    fn len(&self) -> u32 {
        let dword_count = self.s0.is_some() as u32
            + self.s1.is_some() as u32
            + self.s2.is_some() as u32
            + self.s3.is_some() as u32
            + self.s4.is_some() as u32
            + self.s5.is_some() as u32
            + self.s6.is_some() as u32
            + self.s7_depth_offset.is_some() as u32;
        assert!(dword_count > 1);
        dword_count + 1
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum LogicOp {
    Clear = 0x00,
    Nor = 0x01,
    AndInverted = 0x02,
    CopyInverted = 0x03,
    AndReverse = 0x04,
    Invert = 0x05,
    Xor = 0x06,
    Nand = 0x07,
    And = 0x08,
    Equiv = 0x09,
    Noop = 0x0a,
    OrInverted = 0x0b,
    Copy = 0x0c,
    OrReverse = 0x0d,
    Or = 0x0e,
    Set = 0x0f,
}

#[derive(Debug)]
pub struct GpuModes4 {
    pub logic_op: Option<LogicOp>,
    pub stencil_test_mask: Option<u8>,
    pub stencil_write_mask: Option<u8>,
}

impl BatchCommand for GpuModes4 {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_24_p_header(0x0d);

        if let Some(logic_op) = self.logic_op {
            dwords[0] |= 1 << 23;
            dwords[0] |= (logic_op as u32) << 18;
        }

        if let Some(stencil_test_mask) = self.stencil_test_mask {
            dwords[0] |= 1 << 17;
            dwords[0] |= (stencil_test_mask as u32) << 8;
        }

        if let Some(stencil_write_mask) = self.stencil_write_mask {
            dwords[0] |= 1 << 17;
            dwords[0] |= stencil_write_mask as u32;
        }
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug)]
pub struct GpuPixelShaderConstants {
    pub regmask: u32,
    pub consts: Vec<(f32, f32, f32, f32)>,
}

impl BatchCommand for GpuPixelShaderConstants {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_p_header(0x06) | (self.len() - 2);
        dwords[1] = self.regmask;
        for i in 0..self.consts.len() {
            dwords[2 + i * 4] = self.consts[i].0.to_bits();
            dwords[2 + i * 4 + 1] = self.consts[i].1.to_bits();
            dwords[2 + i * 4 + 2] = self.consts[i].2.to_bits();
            dwords[2 + i * 4 + 3] = self.consts[i].3.to_bits();
        }
    }

    fn len(&self) -> u32 {
        (self.consts.len() as u32) * 4 + 2
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuPointRastRule {
    UpperLeft = 0x00,
    UpperRight = 0x01,
    LowerRight = 0x03,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuLineProvokingVertex {
    V0 = 0x00,
    V1 = 0x01,
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum GpuTriFanProvokingVertex {
    V0 = 0x00,
    V1 = 0x01,
    V2 = 0x02,
}

#[derive(Debug)]
pub struct GpuRasterizationRules {
    pub triangle_filter_2x2_disable: bool,
    pub triangle_filter_2x2_modify_enable: bool,
    pub zero_pixel_filter_disable: bool,
    pub zero_pixel_filter_modify_enable: bool,
    pub point_rast_rule_modify_enable: bool,
    pub point_rast_rule: GpuPointRastRule,
    pub snake_walk_disable: bool,
    pub texkill_4d_enable: bool,
    pub texkill_4d_modify_enable: bool,
    pub line_provoking_vertex_modify_enable: bool,
    pub line_provoking_vertex: GpuLineProvokingVertex,
    pub tri_fan_provoking_vertex_modify_enable: bool,
    pub tri_fan_provoking_vertex: GpuTriFanProvokingVertex,
}

impl Default for GpuRasterizationRules {
    fn default() -> Self {
        GpuRasterizationRules {
            triangle_filter_2x2_disable: false,
            triangle_filter_2x2_modify_enable: false,
            zero_pixel_filter_disable: false,
            zero_pixel_filter_modify_enable: false,
            point_rast_rule: GpuPointRastRule::UpperLeft,
            point_rast_rule_modify_enable: false,
            snake_walk_disable: false,
            texkill_4d_enable: false,
            texkill_4d_modify_enable: false,
            line_provoking_vertex: GpuLineProvokingVertex::V0,
            line_provoking_vertex_modify_enable: false,
            tri_fan_provoking_vertex: GpuTriFanProvokingVertex::V0,
            tri_fan_provoking_vertex_modify_enable: false,
        }
    }
}

impl BatchCommand for GpuRasterizationRules {
    fn encode(&self, dwords: &mut [u32]) {
        // TODO: More packing
        dwords[0] = gpu_24_np_header(0x07)
            | (self.triangle_filter_2x2_disable as u32) << 18
            | (self.triangle_filter_2x2_modify_enable as u32) << 17
            | (self.zero_pixel_filter_disable as u32) << 16
            | (self.zero_pixel_filter_modify_enable as u32) << 15
            | (self.point_rast_rule_modify_enable as u32) << 15
            | (self.point_rast_rule as u32) << 13
            | (self.snake_walk_disable as u32) << 12
            | (self.texkill_4d_modify_enable as u32) << 10
            | (self.texkill_4d_enable as u32) << 9
            | (self.line_provoking_vertex_modify_enable as u32) << 8
            | (self.line_provoking_vertex as u32) << 6
            | (self.tri_fan_provoking_vertex_modify_enable as u32) << 5
            | (self.tri_fan_provoking_vertex as u32) << 3;
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Debug)]
pub struct GpuScissorEnable {
    pub modify_enable: bool,
    pub enable: bool,
}

impl BatchCommand for GpuScissorEnable {
    fn encode(&self, dwords: &mut [u32]) {
        // TODO: More packing
        dwords[0] =
            gpu_16_np_header(0x10) | ((self.modify_enable as u32) << 1) | (self.enable as u32);
    }

    fn len(&self) -> u32 {
        1
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct GpuScissorRectangle {
    pub xmin: u16,
    pub ymin: u16,
    pub xmax: u16,
    pub ymax: u16,
}

impl BatchCommand for GpuScissorRectangle {
    fn encode(&self, dwords: &mut [u32]) {
        // TODO: More packing
        dwords[0] = gpu_mw_np_header(0x81) | 1;
        dwords[1] = self.xmin as u32 | ((self.ymin as u32) << 16);
        dwords[2] = self.xmax as u32 | ((self.ymax as u32) << 16);
    }

    fn len(&self) -> u32 {
        3
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct GpuSpanStipple {
    pub enable: bool,
    pub pattern: u16,
}

impl BatchCommand for GpuSpanStipple {
    fn encode(&self, dwords: &mut [u32]) {
        dwords[0] = gpu_mw_np_header(0x83) | 1;
        dwords[1] = (self.enable as u32) << 16 | (self.pattern as u32);
    }

    fn len(&self) -> u32 {
        2
    }
}

custom_derive! {
    #[derive(Debug)]
    #[derive(EnumInnerAsTrait(pub as_batch_command -> &dyn BatchCommand))]
    pub enum Command {
        Noop(MiNoop),
        Flush(MiFlush),
        Primitive(GpuPrimitive),
        AntiAliasing(GpuAntiAliasing),
        BufferInfo(GpuBufferInfo),
        CoordSetBindings(GpuCoordSetBindings),
        ConstantBlendColor(GpuConstantBlendColor),
        DefaultDiffuse(GpuDefaultDiffuse),
        DefaultSpecular(GpuDefaultSpecular),
        DefaultZ(GpuDefaultZ),
        DepthOffsetScale(GpuDepthOffsetScale),
        DepthSubrectangleEnable(GpuDepthSubrectangleEnable),
        DrawingRectangle(GpuDrawingRectangle),
        LoadIndirectState(GpuLoadIndirectState),
        LoadStateImmediate1(GpuLoadStateImmediate1),
        Modes4(GpuModes4),
        PixelShaderConstants(GpuPixelShaderConstants),
        RasterizationRules(GpuRasterizationRules),
        ScissorEnable(GpuScissorEnable),
        ScissorRectangle(GpuScissorRectangle),
        SpanStipple(GpuSpanStipple),
    }
}

fn parse_argb(argb: u32) -> (u8, u8, u8, u8) {
    (
        (argb >> 16) as u8,
        (argb >> 8) as u8,
        argb as u8,
        (argb >> 24) as u8,
    )
}

fn gpu_parse(buf: &DeviceBuf, offset: u32, val: u32) -> Result<Command> {
    let mut val = val;
    let instr = take_bits(&mut val, 24, 28);
    let c = match instr {
        0x06 => {
            let line_cap_aa_width = GpuLineAaWidth::from_u32(take_bits(&mut val, 14, 15)).unwrap();
            let line_cap_aa_width = if take_bool(&mut val, 16) {
                Some(line_cap_aa_width)
            } else {
                None
            };

            let line_aa_width = GpuLineAaWidth::from_u32(take_bits(&mut val, 6, 7)).unwrap();
            let line_aa_width = if take_bool(&mut val, 8) {
                Some(line_aa_width)
            } else {
                None
            };

            if val != 0 {
                bail!("Unknown 3DSTATE_ANTI_ALIASING bits: {:#b}\n", val);
            }
            Command::AntiAliasing(GpuAntiAliasing {
                line_cap_aa_width,
                line_aa_width,
            })
        }

        0x07 => {
            let rast_rules = GpuRasterizationRules {
                triangle_filter_2x2_modify_enable: take_bool(&mut val, 19),
                triangle_filter_2x2_disable: take_bool(&mut val, 18),
                zero_pixel_filter_modify_enable: take_bool(&mut val, 17),
                zero_pixel_filter_disable: take_bool(&mut val, 16),
                point_rast_rule_modify_enable: take_bool(&mut val, 15),
                point_rast_rule: GpuPointRastRule::from_u32(take_bits(&mut val, 13, 14))
                    .context("Parsing point rast rule")?,
                snake_walk_disable: take_bool(&mut val, 12),
                texkill_4d_modify_enable: take_bool(&mut val, 10),
                texkill_4d_enable: take_bool(&mut val, 9),
                line_provoking_vertex_modify_enable: take_bool(&mut val, 8),
                line_provoking_vertex: GpuLineProvokingVertex::from_u32(take_bits(&mut val, 6, 7))
                    .context("Parsing line provoking vertex")?,
                tri_fan_provoking_vertex_modify_enable: take_bool(&mut val, 5),
                tri_fan_provoking_vertex: GpuTriFanProvokingVertex::from_u32(take_bits(
                    &mut val, 3, 4,
                ))
                .context("Parsing triangle provoking vertex")?,
            };

            if val != 0 {
                bail!("Unknown 3DSTATE_RASTERIZATION_RULES bits: {:#b}\n", val);
            }
            Command::RasterizationRules(rast_rules)
        }

        0x0d => {
            let logic_op = if take_bool(&mut val, 23) {
                Some(LogicOp::from_u32(take_bits(&mut val, 18, 21)).context("mapping logic op")?)
            } else {
                None
            };

            let stencil_test_mask = if take_bool(&mut val, 17) {
                Some(take_bits(&mut val, 8, 15) as u8)
            } else {
                None
            };

            let stencil_write_mask = if take_bool(&mut val, 16) {
                Some(take_bits(&mut val, 0, 7) as u8)
            } else {
                None
            };

            if val != 0 {
                bail!("Unknown 3DSTATE_MODES_4 bits: {:#b}\n", val);
            }
            Command::Modes4(GpuModes4 {
                logic_op,
                stencil_test_mask,
                stencil_write_mask,
            })
        }

        0x16 => {
            let c = GpuCoordSetBindings {
                src: [
                    take_bits(&mut val, 0, 2) as u8,
                    take_bits(&mut val, 3, 5) as u8,
                    take_bits(&mut val, 6, 8) as u8,
                    take_bits(&mut val, 9, 11) as u8,
                    take_bits(&mut val, 12, 14) as u8,
                    take_bits(&mut val, 15, 17) as u8,
                    take_bits(&mut val, 18, 20) as u8,
                    take_bits(&mut val, 21, 23) as u8,
                ],
            };

            if val != 0 {
                bail!("Unknown 3DSTATE_COORD_SET_BINDINGS bits: {:#b}\n", val);
            }
            Command::CoordSetBindings(c)
        }

        0x1f => {
            let vertex_indirect = take_bool(&mut val, 23);

            let prim_type = take_bits(&mut val, 18, 22);
            let prim_type = GpuPrimType::from_u32(prim_type).context("parsing primitive type")?;

            if vertex_indirect {
                let len = take_bits(&mut val, 0, 15);
                let access_random = take_bool(&mut val, 17);
                if val != 0 {
                    bail!("Unknown 3DPRIMITIVE bits: {:#b}\n", val);
                }
                Command::Primitive(GpuPrimitive {
                    prim_type,
                    vertex_indirect,
                    access_random,
                    len,
                })
            } else {
                let len = take_bits(&mut val, 0, 17);
                if val != 0 {
                    bail!("Unknown 3DPRIMITIVE bits: {:#b}\n", val);
                }
                Command::Primitive(GpuPrimitive {
                    prim_type,
                    vertex_indirect,
                    access_random: false,
                    len,
                })
            }
        }

        // secondary non-pipelined-commands
        0x1c => match take_bits(&mut val, 19, 23) {
            0x10 => {
                let modify_enable = take_bool(&mut val, 1);
                let enable = take_bool(&mut val, 0);

                if val != 0 {
                    bail!("Unknown 3DSTATE_SCISSOR_ENABLE[0] bits: {:#b}\n", val);
                }

                Command::ScissorEnable(GpuScissorEnable {
                    modify_enable,
                    enable,
                })
            }

            0x11 => {
                let modify_enable = take_bool(&mut val, 1);
                let enable = take_bool(&mut val, 0);

                if val != 0 {
                    bail!(
                        "Unknown 3DSTATE_DEPTH_SUBRECTANGLE_ENABLE[0] bits: {:#b}\n",
                        val
                    );
                }

                Command::DepthSubrectangleEnable(GpuDepthSubrectangleEnable {
                    modify_enable,
                    enable,
                })
            }

            x => bail!("Unknown 3DSTATE 16NP instr: 0x{:x}", x << 16),
        },

        // non-pipelined commands
        0x1d => match take_bits(&mut val, 16, 23) {
            0x04 => {
                let len = take_bits(&mut val, 0, 3);

                let mut next_offset = offset + 4;

                let s0 = if take_bool(&mut val, 4) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S0")?;
                    next_offset += 4;

                    let vertex_buffer_address = take_bits(&mut dword, 2, 28);
                    let vertex_cache_invalidate_disable = take_bool(&mut dword, 0);

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S0 bits: {:#b}\n", val);
                    }

                    Some(GpuLoadStateImmediate1S0 {
                        vertex_buffer_address,
                        vertex_cache_invalidate_disable,
                    })
                } else {
                    None
                };

                let s1 = if take_bool(&mut val, 5) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S1")?;
                    next_offset += 4;

                    let vb_width = take_bits(&mut dword, 24, 29);
                    let vb_pitch = take_bits(&mut dword, 16, 21);
                    let vb_max_index = take_bits(&mut dword, 0, 15) as u16;

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S1 bits: {:#b}\n", val);
                    }

                    Some(GpuLoadStateImmediate1S1 {
                        vb_width,
                        vb_pitch,
                        vb_max_index,
                    })
                } else {
                    None
                };

                let s2 = if take_bool(&mut val, 6) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S2")?;
                    next_offset += 4;

                    let mut tc_formats = [TCFormat::TexCoordFormatNotPresent; 8];
                    for i in 0..8 {
                        let bits = take_bits(&mut dword, 4 * i, 4 * i + 3);
                        tc_formats[i as usize] = TCFormat::from_u32(bits)
                            .with_context(|| format!("Mapping TC format bits {:b}", bits))?
                    }

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S2 bits: {:#b}\n", val);
                    }

                    Some(GpuLoadStateImmediate1S2 { tc_formats })
                } else {
                    None
                };

                let s3 = if take_bool(&mut val, 7) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S3")?;
                    next_offset += 4;

                    let mut tc_wrap_shortest_x = [false; 8];
                    let mut tc_wrap_shortest_y = [false; 8];
                    let mut tc_wrap_shortest_z = [false; 8];
                    let mut tc_perspective_correction = [false; 8];

                    for i in 0..8u32 {
                        tc_wrap_shortest_x[i as usize] = take_bool(&mut dword, 4 * i + 3);
                        tc_wrap_shortest_y[i as usize] = take_bool(&mut dword, 4 * i + 2);
                        tc_wrap_shortest_z[i as usize] = take_bool(&mut dword, 4 * i + 1);
                        tc_perspective_correction[i as usize] = take_bool(&mut dword, 4 * i);
                    }

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S3 bits: {:#b}\n", val);
                    }

                    Some(GpuLoadStateImmediate1S3 {
                        tc_wrap_shortest_x,
                        tc_wrap_shortest_y,
                        tc_wrap_shortest_z,
                        tc_perspective_correction,
                    })
                } else {
                    None
                };

                let s4 = if take_bool(&mut val, 8) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S4")?;
                    next_offset += 4;

                    let s4 = GpuLoadStateImmediate1S4 {
                        point_width: take_bits(&mut dword, 23, 31),
                        line_width: (take_bits(&mut dword, 19, 22) as f32) / 2.0,
                        alpha_shade_flat: take_bool(&mut dword, 18),
                        fog_shade_flat: take_bool(&mut dword, 17),
                        specular_shade_flat: take_bool(&mut dword, 16),
                        color_shade_flat: take_bool(&mut dword, 15),
                        cull_mode: CullMode::from_u32(take_bits(&mut dword, 13, 14))
                            .context("mapping cull mode")?,
                        point_width_per_vertex: take_bool(&mut dword, 12),
                        specular: take_bool(&mut dword, 11),
                        diffuse: take_bool(&mut dword, 10),
                        local_depth_offset_per_vertex: take_bool(&mut dword, 9),
                        position_mask: PositionMask::from_u32(take_bits(&mut dword, 6, 8))
                            .context("mapping position mask")?,
                        force_default_diffuse: take_bool(&mut dword, 5),
                        force_default_specular: take_bool(&mut dword, 4),
                        local_depth_offset: take_bool(&mut dword, 3),
                        fog: take_bool(&mut dword, 2),
                        point_sprite: take_bool(&mut dword, 1),
                        line_aa: take_bool(&mut dword, 0),
                    };

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S4 bits: {:#b}\n", val);
                    }

                    Some(s4)
                } else {
                    None
                };

                let s5 = if take_bool(&mut val, 9) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S5")?;
                    next_offset += 4;

                    let s5 = GpuLoadStateImmediate1S5 {
                        write_a: !take_bool(&mut dword, 31),
                        write_r: !take_bool(&mut dword, 30),
                        write_g: !take_bool(&mut dword, 29),
                        write_b: !take_bool(&mut dword, 28),

                        force_default_point_width: take_bool(&mut dword, 27),
                        last_pixel: take_bool(&mut dword, 26),
                        global_depth_offset: take_bool(&mut dword, 25),
                        fog: take_bool(&mut dword, 24),

                        stencil_ref: take_bits(&mut dword, 16, 23) as u8,

                        stencil_func: CompareFunc::from_u32(take_bits(&mut dword, 13, 15))
                            .context("mapping stencil func")?,
                        stencil_fail_op: StencilOp::from_u32(take_bits(&mut dword, 10, 12))
                            .context("mapping stencil fail")?,
                        stencil_zfail_op: StencilOp::from_u32(take_bits(&mut dword, 7, 9))
                            .context("mapping stencil zfail")?,
                        stencil_zpass_op: StencilOp::from_u32(take_bits(&mut dword, 4, 6))
                            .context("mapping stencil zpass")?,
                        stencil_write: take_bool(&mut dword, 3),
                        stencil_test: take_bool(&mut dword, 2),
                        color_dither: take_bool(&mut dword, 1),
                        logic_op: take_bool(&mut dword, 0),
                    };

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S5 bits: {:#b}\n", val);
                    }

                    Some(s5)
                } else {
                    None
                };

                let s6 = if take_bool(&mut val, 10) {
                    let mut dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S6")?;
                    next_offset += 4;

                    let mut s6 = GpuLoadStateImmediate1S6 {
                        alpha_test: take_bool(&mut dword, 31),
                        alpha_func: CompareFunc::from_u32(take_bits(&mut dword, 28, 30))
                            .context("mapping alpha func")?,
                        alpha_ref: take_bits(&mut dword, 20, 27) as u8,

                        depth_test: take_bool(&mut dword, 19),
                        depth_func: CompareFunc::from_u32(take_bits(&mut dword, 16, 18))
                            .context("mapping depth func")?,

                        blend: take_bool(&mut dword, 15),
                        blend_func: BlendFunc::from_u32(take_bits(&mut dword, 12, 14))
                            .context("mapping blend func")?,

                        src_blend: BlendFactor::Zero,
                        dst_blend: BlendFactor::Zero,

                        depth_write: take_bool(&mut dword, 3),
                        color_write: take_bool(&mut dword, 2),

                        provoking_vertex: ProvokingVertex::from_u32(take_bits(&mut dword, 0, 2))
                            .context("mapping provoking vertex")?,
                    };

                    let src_blend = take_bits(&mut dword, 8, 11);
                    let dst_blend = take_bits(&mut dword, 4, 7);
                    if s6.blend {
                        // These values are ignored in HW if blend is not set.
                        s6.src_blend =
                            BlendFactor::from_u32(src_blend).context("mapping src_blend")?;
                        s6.dst_blend =
                            BlendFactor::from_u32(dst_blend).context("mapping dst_blend")?;
                    }

                    if dword != 0 {
                        bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE.S6 bits: {:#b}\n", val);
                    }

                    Some(s6)
                } else {
                    None
                };

                let s7 = if take_bool(&mut val, 11) {
                    let dword = buf
                        .read_dword(next_offset)
                        .context("3DSTATE_LOAD_STATE_IMMEDIATE.S7")?;
                    Some(f32::from_bits(dword))
                } else {
                    None
                };

                if val != 0 {
                    bail!("Unknown 3DSTATE_LOAD_STATE_IMMEDIATE[0] bits: {:#b}\n", val);
                }

                let dwords_parsed = (next_offset - offset) / 4;
                if len + 2 != dwords_parsed {
                    bail!(
                        "len {} doesn't match number of dwords parsed {}",
                        len,
                        dwords_parsed
                    );
                }
                Command::LoadStateImmediate1(GpuLoadStateImmediate1 {
                    s0,
                    s1,
                    s2,
                    s3,
                    s4,
                    s5,
                    s6,
                    s7_depth_offset: s7,
                })
            }

            0x06 => {
                let len = take_bits(&mut val, 0, 7);

                let regmask = buf
                    .read_dword(offset + 4)
                    .context("3DSTATE_PIXEL_SHADER_CONSTANTS[1]")?;

                let vec4_len = regmask.count_ones();
                if len != vec4_len * 4 {
                    bail!("regmask 0x{:x} doesn't match packet len {}", regmask, len);
                }

                if val != 0 {
                    bail!(
                        "Unknown 3DSTATE_PIXEL_SHADER_CONSTANTS[0] bits: {:#b}\n",
                        val
                    );
                }

                let mut constants = GpuPixelShaderConstants {
                    regmask,
                    consts: Vec::new(),
                };

                for i in 0..vec4_len {
                    constants.consts.push((
                        f32::from_bits(
                            buf.read_dword(offset + 8 + i * 16)
                                .context("3DSTATE_PIXEL_SHADER_CONSTANTS[4n+0]")?,
                        ),
                        f32::from_bits(
                            buf.read_dword(offset + 8 + i * 16 + 4)
                                .context("3DSTATE_PIXEL_SHADER_CONSTANTS[4n+1]")?,
                        ),
                        f32::from_bits(
                            buf.read_dword(offset + 8 + i * 16 + 8)
                                .context("3DSTATE_PIXEL_SHADER_CONSTANTS[4n+2]")?,
                        ),
                        f32::from_bits(
                            buf.read_dword(offset + 8 + i * 16 + 12)
                                .context("3DSTATE_PIXEL_SHADER_CONSTANTS[4n+3]")?,
                        ),
                    ))
                }

                Command::PixelShaderConstants(constants)
            }

            0x07 => {
                if val != 0 {
                    bail!("Unknown 3DSTATE_LOAD_INDIRECT_STATE bits: {:#b}\n", val);
                }
                Command::LoadIndirectState(GpuLoadIndirectState {})
            }

            0x80 => {
                take_fixed_len(&mut val, 3).context("Parsing 3DSTATE_DRAWING_RECTANGLE")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DRAWING_RECTANGLE[0] bits: {:#b}\n", val);
                }

                let val = buf.read_dword(offset + 4)?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DRAWING_RECTANGLE[1] bits: {:#b}\n", val);
                }

                let dw2 = buf
                    .read_dword(offset + 8)
                    .context("3DSTATE_DRAWING_RECTANGLE[2]")?;
                let dw3 = buf
                    .read_dword(offset + 12)
                    .context("3DSTATE_DRAWING_RECTANGLE[3]")?;
                let dw4 = buf
                    .read_dword(offset + 16)
                    .context("3DSTATE_DRAWING_RECTANGLE[4]")?;

                Command::DrawingRectangle(GpuDrawingRectangle {
                    clip_xmin: dw2 as u16,
                    clip_ymin: (dw2 >> 16) as u16,
                    clip_xmax: dw3 as u16,
                    clip_ymax: (dw3 >> 16) as u16,
                    draw_x: dw4 as i16,
                    draw_y: (dw4 >> 16) as i16,
                })
            }

            0x81 => {
                take_fixed_len(&mut val, 1).context("Parsing 3DSTATE_SCISSOR_RECTANGLE")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_SCISSOR_RECTANGLE[0] bits: {:#b}\n", val);
                }

                let dw1 = buf
                    .read_dword(offset + 4)
                    .context("3DSTATE_SCISSOR_RECTANGLE[1]")?;
                let dw2 = buf
                    .read_dword(offset + 8)
                    .context("3DSTATE_SCISSOR_RECTANGLE[2]")?;

                Command::ScissorRectangle(GpuScissorRectangle {
                    xmin: dw1 as u16,
                    ymin: (dw1 >> 16) as u16,
                    xmax: dw2 as u16,
                    ymax: (dw2 >> 16) as u16,
                })
            }

            0x83 => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_SPAN_STIPPLE")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_SPAN_STIPPLE[0] bits: {:#b}\n", val);
                }

                let mut dw1 = buf
                    .read_dword(offset + 4)
                    .context("3DSTATE_SPAN_STIPPLE[1]")?;

                let enable = take_bool(&mut dw1, 16);
                let pattern = take_bits(&mut dw1, 0, 15) as u16;

                if dw1 != 0 {
                    bail!("Unknown 3DSTATE_SPAN_STIPPLE[0] bits: {:#b}\n", val);
                }

                Command::SpanStipple(GpuSpanStipple { enable, pattern })
            }

            0x88 => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_CONSTANT_BLEND_COLOR")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_CONSTANT_BLEND_COLOR[0] bits: {:#b}\n", val);
                }

                Command::ConstantBlendColor(GpuConstantBlendColor {
                    rgba: parse_argb(buf.read_dword(offset + 4)?),
                })
            }

            0x8e => {
                take_fixed_len(&mut val, 1).context("Parsing 3DSTATE_BUFFER_INFO")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_BUFFER_INFO dw0 bits: {:#b}\n", val);
                }

                let mut val = buf.read_dword(offset + 4)?;
                let buffer_id = GpuBufferID::from_u32(take_bits(&mut val, 24, 27))
                    .context("Parsing buffer ID")?;
                let aux_buffer_id = take_bool(&mut val, 28);
                let use_fence = take_bool(&mut val, 23);
                let tiled = take_bool(&mut val, 22);
                let tiled_y = take_bool(&mut val, 21);
                // HW value is in dwords, expand to bytes.
                let pitch = take_bits(&mut val, 2, 13) << 2;
                if val != 0 {
                    bail!("Unknown 3DSTATE_BUFFER_INFO dw1 bits: {:#b}\n", val);
                }

                let mut val = buf.read_dword(offset + 8)?;
                // HW value is in dwords, expand to bytes.
                let offset = take_bits(&mut val, 2, 28) << 2;
                if val != 0 {
                    bail!("Unknown 3DSTATE_BUFFER_INFO dw2 bits: {:#b}\n", val);
                }

                Command::BufferInfo(GpuBufferInfo {
                    aux_buffer_id,
                    buffer_id,
                    use_fence,
                    tiled,
                    tiled_y,
                    offset,
                    pitch,
                })
            }

            0x99 => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_DEFAULT_DIFFUSE")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DEFAULT_DIFFUSE[0] bits: {:#b}\n", val);
                }

                Command::DefaultDiffuse(GpuDefaultDiffuse {
                    rgba: parse_argb(buf.read_dword(offset + 4)?),
                })
            }

            0x9a => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_DEFAULT_SPECULAR")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DEFAULT_SPECULAR[0] bits: {:#b}\n", val);
                }

                Command::DefaultSpecular(GpuDefaultSpecular {
                    rgba: parse_argb(buf.read_dword(offset + 4)?),
                })
            }

            0x98 => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_DEFAULT_Z")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DEFAULT_Z[0] bits: {:#b}\n", val);
                }

                Command::DefaultZ(GpuDefaultZ {
                    z: f32::from_bits(buf.read_dword(offset + 4)?),
                })
            }

            0x97 => {
                take_fixed_len(&mut val, 0).context("Parsing 3DSTATE_DEPTH_OFFSET_SCALE")?;
                if val != 0 {
                    bail!("Unknown 3DSTATE_DEPTH_OFFSET_SCALE[0] bits: {:#b}\n", val);
                }

                Command::DepthOffsetScale(GpuDepthOffsetScale {
                    scale: f32::from_bits(buf.read_dword(offset + 4)?),
                })
            }

            x => bail!("Unknown 3DSTATE MW P/NP instr: 0x{:x}", x << 16),
        },
        _ => bail!("Unknown 3D instr 0x{:x}", instr),
    };

    Ok(c)
}

impl Command {
    pub fn parse(buf: &DeviceBuf, offset: u32) -> Result<Command> {
        let mut val = buf.read_dword(offset)?;
        let c = match take_bits(&mut val, 29, 31) {
            0x0 => mi_parse(buf, offset, val).context("Parsing MI subcommand")?,
            0x3 => gpu_parse(buf, offset, val).context("Parsing 3D subcommand")?,
            x => bail!("Unknown instruction type {}", x),
        };

        Ok(c)
    }
}

impl BatchCommand for Command {
    fn encode(&self, dwords: &mut [u32]) {
        self.as_batch_command().encode(dwords)
    }

    fn len(&self) -> u32 {
        self.as_batch_command().len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode() {
        let mut dword = vec![0u32; 1];
        MiNoop {}.encode(&mut dword);
        assert_eq!(dword[0], 0);
    }

    #[test]
    #[ignore] /* about 10 seconds */
    fn roundtrip_mi() {
        let mut buf = DeviceBuf(vec![0u8; 4096]);

        for mi_instr in [0x0, 0x4].iter() {
            for val in 0..(1 << 23) {
                let dword = (mi_instr << 23) | val;
                buf.write_dword(0, dword);
                if let Ok(parsed) = Command::parse(&buf, 0) {
                    let mut encoded = [0u32];
                    parsed.encode(&mut encoded);
                    assert_eq!(dword, encoded[0]);
                }
            }
        }
    }

    #[test]
    fn parse_invalid() {
        let mut buf = DeviceBuf(vec![0u8; 4096]);
        buf.write_dword(0, 0xdddddddd);
        assert!(Command::parse(&buf, 0).is_err());
    }
}
