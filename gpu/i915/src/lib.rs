#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate glium;
#[macro_use]
extern crate custom_derive;
#[macro_use]
extern crate enum_derive;

#[cfg(any(cargo_c, feature = "capi"))]
mod capi;
mod command;

use anyhow::{bail, Context, Result};
use command::*;
use glium::{DrawParameters, Surface};
use glutin::platform::unix::{EventLoopExtUnix, HeadlessContextExt};

use log::*;

#[derive(Debug)]
pub struct DeviceBuf(Vec<u8>);

impl DeviceBuf {
    pub fn read_dword(&self, offset: u32) -> Result<u32> {
        let offset = offset as usize;
        if offset + 4 > self.0.len() {
            bail!("Reading from 0x{:08x} in buffer", offset);
        }

        Ok(self.0[offset] as u32
            | ((self.0[offset + 1] as u32) << 8)
            | ((self.0[offset + 2] as u32) << 16)
            | ((self.0[offset + 3] as u32) << 24))
    }

    pub fn write_dword(&mut self, offset: u32, val: u32) {
        let offset = offset as usize;
        self.0[offset] = val as u8;
        self.0[offset + 1] = (val >> 8) as u8;
        self.0[offset + 2] = (val >> 16) as u8;
        self.0[offset + 3] = (val >> 24) as u8;
    }
}

pub struct State {
    memory: Memory,

    cp_offset: u32,

    color_info: Option<GpuBufferInfo>,
    depth_info: Option<GpuBufferInfo>,
    draw_rect: Option<GpuDrawingRectangle>,

    constant_blend_color: (u8, u8, u8, u8),
    default_diffuse: (u8, u8, u8, u8),
    default_specular: (u8, u8, u8, u8),
    default_z: f32,

    depth_offset_scale: f32,

    consts: [(f32, f32, f32, f32); 32],

    color: Option<glium::texture::Texture2d>,

    point_rast_rule: GpuPointRastRule,
    texkill_4d: bool,
    line_provoking_vertex: GpuLineProvokingVertex,
    tri_fan_provoking_vertex: GpuTriFanProvokingVertex,

    scissor_enable: bool,
    scissor: GpuScissorRectangle,

    vertex_buffer_address: u32,
    vb_width: u32,
    vb_pitch: u32,
    vb_max_index: u32,

    tex_to_vb_map: [u8; 8],

    stencil_test_mask: u8,
    stencil_write_mask: u8,
    logic_op: LogicOp,
}

impl State {
    pub fn cp_peek(&self) -> Result<u32> {
        self.memory.read_dword(self.cp_offset)
    }

    pub fn cp_next(&mut self) -> Result<u32> {
        let val = self.cp_peek()?;
        self.cp_offset += 4;
        Ok(val)
    }

    pub fn cp_next_f(&mut self) -> Result<f32> {
        Ok(f32::from_bits(self.cp_next()?))
    }

    pub fn cp_parse_next(&mut self) -> Result<Command> {
        let l = self.memory.lookup(self.cp_offset)?;
        let c = Command::parse(l.0, l.1);
        debug!(
            "CP @ 0x{:08x} = 0x{:x} {:?}",
            self.cp_offset,
            self.cp_peek().unwrap_or(0xdeadbeef),
            &c
        );
        let c = c?;
        self.cp_offset += c.len() * 4;
        Ok(c)
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive, ToPrimitive, PartialEq)]
pub enum Devid {
    I945G = 0x2772,
    I945GM = 0x27a2,
    PnvGM = 0xa011,
    PnvG = 0xa001,
}

pub struct Device {
    // We'll use it later, I'm sure, but we want it passed in by the C API
    #[allow(dead_code)]
    devid: Devid,

    glium: glium::backend::glutin::headless::Headless,
}

impl Device {
    pub fn new(devid: Devid) -> Result<Device> {
        let el: glutin::event_loop::EventLoop<()> = glutin::event_loop::EventLoop::new_any_thread();
        let cb = glutin::ContextBuilder::new()
            .with_gl_profile(glutin::GlProfile::Compatibility)
            .with_gl(glutin::GlRequest::Latest);
        let context = cb
            .build_surfaceless(&el)
            .context("Creating surfaceless context")?;

        let glium =
            glium::backend::glutin::headless::Headless::new(context).context("Setting up glium")?;

        Ok(Device { devid, glium })
    }

    fn primitive(&mut self, state: &mut State, p: GpuPrimitive) -> Result<()> {
        #[derive(Copy, Clone)]
        struct Vertex {
            position: [f32; 4],
        }

        let mut vertex_data: Vec<Vertex> = Vec::new();
        let mut index_data = Vec::new();
        if p.vertex_indirect {
            panic!("only inline prims supported so far");
        } else {
            if p.access_random {
                panic!("index buffer unsupported so far");
            }

            implement_vertex!(Vertex, position);

            for i in 0..p.len {
                let vertex = Vertex {
                    position: [
                        state.cp_next_f().context("X fetch")?,
                        state.cp_next_f().context("Y fetch")?,
                        0.0,
                        1.0,
                    ],
                };

                vertex_data.push(vertex);
                /* XXX: Z, RHW, psiz, etc. */

                index_data.push(i);
            }
        }

        let prim_type = match p.prim_type {
            GpuPrimType::TriList => glium::index::PrimitiveType::TrianglesList,
            GpuPrimType::TriStrip => glium::index::PrimitiveType::TriangleStrip,
            GpuPrimType::TriFan => glium::index::PrimitiveType::TriangleFan,
            _ => panic!("unhandled prim type"),
        };

        let vertex_buffer =
            glium::VertexBuffer::new(&self.glium, &vertex_data).context("creating VB")?;
        let index_buffer =
            glium::IndexBuffer::new(&self.glium, prim_type, &index_data).context("Creating IB")?;

        let width = state.draw_rect.as_ref().unwrap().clip_xmax as u32;
        let height = state.draw_rect.as_ref().unwrap().clip_ymax as u32;

        state.color = Some(
            glium::Texture2d::empty_with_format(
                &self.glium,
                glium::texture::UncompressedFloatFormat::U8U8U8U8,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height,
            )
            .unwrap(),
        );

        let mut fb =
            glium::framebuffer::SimpleFrameBuffer::new(&self.glium, state.color.as_ref().unwrap())
                .context("creating framebuffer")?;

        let prog = program!(&self.glium,
            140 => {
                vertex: "
                    #version 140
                    in vec4 position;
                    void main() {
                        gl_Position = position;
                    }
                ",
                fragment: "
                    #version 140
                    out vec4 out_color;
                    void main() {
                        out_color = vec4(1,0,0,0);
                    }
                ",
            },
        )
        .context("creating shader")?;

        let stencil = glium::draw_parameters::Stencil {
            // TODO: Fill in more state.
            write_mask_clockwise: state.stencil_test_mask as u32,
            ..Default::default()
        };
        let draw_parameters: DrawParameters = DrawParameters {
            stencil,
            ..Default::default()
        };
        let uniforms = uniform! {};
        fb.draw(
            &vertex_buffer,
            &index_buffer,
            &prog,
            &uniforms,
            &draw_parameters,
        )
        .context("draw")?;

        Ok(())
    }

    pub fn exec(&mut self, exec: Exec) -> Result<State> {
        let cp_offset = exec.memory.bufs[exec.batch_index].offset + exec.batch_offset;
        let cp_end = cp_offset + exec.batch_len;

        let mut state = State {
            cp_offset,
            memory: exec.memory,
            color_info: None,
            depth_info: None,
            draw_rect: None,
            color: None,

            consts: [(0.0, 0.0, 0.0, 0.0); 32],

            constant_blend_color: (0u8, 0u8, 0u8, 0u8),
            default_diffuse: (0u8, 0u8, 0u8, 0u8),
            default_specular: (0u8, 0u8, 0u8, 0u8),
            default_z: 0.0,

            depth_offset_scale: 0.0,

            scissor_enable: true,
            scissor: GpuScissorRectangle {
                xmin: 0,
                ymin: 0,
                xmax: 0,
                ymax: 0,
            },

            point_rast_rule: GpuPointRastRule::UpperLeft,
            line_provoking_vertex: GpuLineProvokingVertex::V0,
            tri_fan_provoking_vertex: GpuTriFanProvokingVertex::V0,
            texkill_4d: false,

            tex_to_vb_map: [0u8; 8],

            vertex_buffer_address: 0,
            vb_width: 0,
            vb_pitch: 0,
            vb_max_index: 0,

            stencil_test_mask: 0,
            stencil_write_mask: 0,
            logic_op: LogicOp::Copy,
        };

        while state.cp_offset != cp_end {
            match state.cp_parse_next()? {
                Command::Noop(_) => {}

                Command::Flush(_) => {
                    if let Some(color) = state.color.as_ref() {
                        let draw_rect = state.draw_rect.as_ref().unwrap();

                        let width = draw_rect.clip_xmax as u32;
                        let height = draw_rect.clip_ymax as u32;

                        if let Some(color_info) = state.color_info.as_ref() {
                            let readpix = color.read::<Vec<Vec<(u8, u8, u8, u8)>>>();
                            let cpp = 4;
                            let offset = color_info.offset;
                            let pitch = color_info.pitch;

                            for y in 0..height {
                                for x in 0..width {
                                    state.memory.write_dword(
                                        offset + y * pitch + x * cpp,
                                        ((readpix[y as usize][x as usize].3 as u32) << 24)
                                            | ((readpix[y as usize][x as usize].0 as u32) << 16)
                                            | ((readpix[y as usize][x as usize].1 as u32) << 8)
                                            | (readpix[y as usize][x as usize].2 as u32),
                                    )?;
                                }
                            }
                        }
                    }
                }

                Command::Primitive(p) => {
                    self.primitive(&mut state, p)?;
                }

                Command::BufferInfo(info) => match info.buffer_id {
                    GpuBufferID::Color => state.color_info = Some(info),
                    GpuBufferID::Depth => state.depth_info = Some(info),
                    x => bail!("Unsupported buffer id {:?}", x),
                },

                Command::AntiAliasing(_) => {}
                Command::CoordSetBindings(csb) => state.tex_to_vb_map = csb.src,
                Command::ConstantBlendColor(blend) => state.constant_blend_color = blend.rgba,
                Command::DefaultDiffuse(diff) => state.default_diffuse = diff.rgba,
                Command::DefaultSpecular(spec) => state.default_specular = spec.rgba,
                Command::DefaultZ(z) => state.default_z = z.z,
                Command::DepthOffsetScale(scale) => state.depth_offset_scale = scale.scale,
                Command::DrawingRectangle(rect) => state.draw_rect = Some(rect),

                Command::DepthSubrectangleEnable(e) => {
                    if !e.modify_enable {
                        bail!(
                            "3DSTATE_DEPTH_SUBRECTANGLE_ENABLE emitted without modify_enable set."
                        );
                    }
                    if e.enable {
                        bail!("3DSTATE_DEPTH_SUBRECTANGLE_ENABLE enabled unsupported");
                    }
                }

                Command::LoadIndirectState(_) => {}

                Command::LoadStateImmediate1(lsi1) => {
                    if let Some(s0) = lsi1.s0 {
                        state.vertex_buffer_address = s0.vertex_buffer_address;
                    }
                    if let Some(s1) = lsi1.s1 {
                        state.vb_width = s1.vb_width;
                        state.vb_pitch = s1.vb_pitch;
                        state.vb_max_index = s1.vb_max_index as u32;
                    }
                }

                Command::PixelShaderConstants(c) => {
                    let mut regmask = c.regmask;
                    for vals in c.consts {
                        let reg = regmask.trailing_zeros();
                        state.consts[reg as usize] = vals;
                        regmask &= !(1 << reg);
                    }
                }

                Command::RasterizationRules(rules) => {
                    if rules.triangle_filter_2x2_modify_enable && rules.triangle_filter_2x2_disable
                    {
                        bail!("2x2 triangle filter disable not supported.");
                    }
                    if rules.zero_pixel_filter_modify_enable && rules.zero_pixel_filter_disable {
                        bail!("Zero pixel filter disable not supported.");
                    }

                    if rules.point_rast_rule_modify_enable {
                        state.point_rast_rule = rules.point_rast_rule;
                    }
                    if rules.texkill_4d_modify_enable {
                        state.texkill_4d = rules.texkill_4d_enable;
                    }
                    if rules.line_provoking_vertex_modify_enable {
                        state.line_provoking_vertex = rules.line_provoking_vertex;
                    }
                    if rules.tri_fan_provoking_vertex_modify_enable {
                        state.tri_fan_provoking_vertex = rules.tri_fan_provoking_vertex;
                    }
                }

                Command::Modes4(modes4) => {
                    if let Some(logic_op) = modes4.logic_op {
                        state.logic_op = logic_op;
                    }
                    if let Some(stencil_test_mask) = modes4.stencil_test_mask {
                        state.stencil_test_mask = stencil_test_mask;
                    }
                    if let Some(stencil_write_mask) = modes4.stencil_write_mask {
                        state.stencil_write_mask = stencil_write_mask;
                    }
                }

                Command::ScissorEnable(e) => {
                    if !e.modify_enable {
                        bail!("3DSTATE_SCISSOR_ENABLE emitted without modify_enable set.");
                    }
                    state.scissor_enable = e.enable
                }
                Command::ScissorRectangle(rect) => state.scissor = rect,

                Command::SpanStipple(stip) => {
                    if stip.enable {
                        bail!("3DSTATE_SPAN_STIPPLE enabled, not yet supported");
                    }
                }
            }
        }

        Ok(state)
    }
}

#[derive(Debug)]
pub struct ExecBuf {
    data: DeviceBuf,
    offset: u32,
}

// Struct representing the state of the device virtual address space.
#[derive(Debug)]
pub struct Memory {
    pub bufs: Vec<ExecBuf>,
}

impl Memory {
    pub fn read_dword(&self, offset: u32) -> Result<u32> {
        for buf in &self.bufs {
            if offset >= buf.offset && offset < buf.offset + (buf.data.0.len() as u32) {
                return buf.data.read_dword(offset - buf.offset);
            }
        }
        bail!("fault reading from 0x{:x} ", offset)
    }

    pub fn write_dword(&mut self, offset: u32, val: u32) -> Result<()> {
        for buf in self.bufs.iter_mut() {
            if offset >= buf.offset && offset < buf.offset + (buf.data.0.len() as u32) {
                buf.data.write_dword(offset - buf.offset, val);
                return Ok(());
            }
        }
        bail!("fault writing to 0x{:x} (value 0x{:x})", offset, val)
    }

    pub fn lookup(&self, offset: u32) -> Result<(&DeviceBuf, u32)> {
        for buf in &self.bufs {
            if offset >= buf.offset && offset < buf.offset + (buf.data.0.len() as u32) {
                return Ok((&buf.data, offset - buf.offset));
            }
        }
        bail!("fault reading from 0x{:x} ", offset)
    }
}
#[derive(Debug)]
pub struct Exec {
    pub memory: Memory,
    pub batch_index: usize,
    pub batch_offset: u32,
    pub batch_len: u32,
}

#[cfg(test)]
mod tests {
    use super::*;

    // Helper for building up exec buffers for testing
    struct ExecBuilder {
        exec: Exec,

        next_offset: u32,
    }

    impl ExecBuilder {
        fn new() -> ExecBuilder {
            let _ = env_logger::builder().is_test(true).try_init();

            let mut build = ExecBuilder {
                exec: Exec {
                    memory: Memory { bufs: Vec::new() },

                    batch_index: 0,
                    batch_offset: 0,
                    batch_len: 0,
                },

                next_offset: 4096, // Don't allocate the 0-address page, to catch faults.
            };

            /* Allocate batch */
            build.new_buf(4096);

            build
        }

        fn new_buf(&mut self, size: u32) -> usize {
            let index = self.exec.memory.bufs.len();
            let buf = ExecBuf {
                data: DeviceBuf(vec![0u8; size as usize]),
                offset: self.next_offset,
            };
            self.exec.memory.bufs.push(buf);

            self.next_offset += size;

            //Pad between the buffers to catch overflows.
            self.next_offset += 4096;

            index
        }

        fn batch_dw(&mut self, val: u32) {
            self.exec.memory.bufs[self.exec.batch_index]
                .data
                .write_dword(self.exec.batch_offset + self.exec.batch_len, val);
            self.exec.batch_len += 4;
        }

        fn batch_f(&mut self, val: f32) {
            self.batch_dw(val.to_bits());
        }

        fn batch_command<C: BatchCommand>(&mut self, cmd: &C) {
            let len = cmd.len();
            let mut dwords = vec![0u32; len as usize];
            cmd.encode(&mut dwords);
            for dword in dwords {
                self.batch_dw(dword);
            }
        }

        fn finalize(self) -> Exec {
            self.exec
        }
    }

    fn exec_single_command<C: BatchCommand>(c: C) -> Result<State> {
        let mut build = ExecBuilder::new();
        build.batch_command(&c);
        Device::new(Devid::I945GM)?.exec(build.finalize())
    }

    #[test]
    fn noop() -> Result<()> {
        let mut build = ExecBuilder::new();
        build.batch_command(&MiNoop {});
        assert!(Device::new(Devid::I945GM)?.exec(build.finalize()).is_ok());
        Ok(())
    }

    #[test]
    fn bad_op() -> Result<()> {
        let mut build = ExecBuilder::new();
        build.batch_dw(0xd0d0d0d0);
        assert!(Device::new(Devid::I945GM)?.exec(build.finalize()).is_err());
        Ok(())
    }

    #[test]
    fn devicebuf_readwrite() -> Result<()> {
        let mut buf = DeviceBuf(vec![0u8; 8]);
        buf.write_dword(0, 0x01020304);
        buf.write_dword(4, 0x05060708);
        assert_eq!(buf.0[0], 0x04); /* little endian */
        assert_eq!(buf.read_dword(0)?, 0x01020304);
        assert_eq!(buf.read_dword(4)?, 0x05060708);
        Ok(())
    }

    #[test]
    fn constant_blend_color() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuConstantBlendColor {
                rgba: (9, 10, 11, 12)
            })?
            .constant_blend_color,
            (9, 10, 11, 12),
        );
        Ok(())
    }

    #[test]
    fn antialiasing() -> Result<()> {
        exec_single_command(GpuAntiAliasing {
            line_cap_aa_width: None,
            line_aa_width: None,
        })
        .map(|_| ())
    }

    #[test]
    fn coord_set_bindings() -> Result<()> {
        let map = [0, 1, 2, 3, 4, 5, 6, 7];
        assert_eq!(
            exec_single_command(GpuCoordSetBindings { src: map })?.tex_to_vb_map,
            map
        );

        let map = [7, 6, 5, 4, 3, 2, 1, 0];
        assert_eq!(
            exec_single_command(GpuCoordSetBindings { src: map })?.tex_to_vb_map,
            map
        );
        Ok(())
    }

    #[test]
    fn default_diffuse() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuDefaultDiffuse { rgba: (1, 2, 3, 4) })?.default_diffuse,
            (1, 2, 3, 4),
        );
        Ok(())
    }

    #[test]
    fn default_specular() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuDefaultSpecular { rgba: (5, 6, 7, 8) })?.default_specular,
            (5, 6, 7, 8)
        );
        Ok(())
    }

    #[test]
    fn depth_subrectangle_enable() -> Result<()> {
        assert!(exec_single_command(GpuDepthSubrectangleEnable {
            modify_enable: true,
            enable: false
        })
        .is_ok());
        // The spec says to never set this value and doesn't specify what it
        // does.  Assert that it throws an error.
        assert!(exec_single_command(GpuDepthSubrectangleEnable {
            modify_enable: true,
            enable: true
        })
        .is_err());
        // Emit a nonsense depth subrect enable command and make the emulator throws
        // an error.  No driver should do this, even though it's a valid packet.
        assert!(exec_single_command(GpuDepthSubrectangleEnable {
            modify_enable: false,
            enable: false
        })
        .is_err());
        Ok(())
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn default_z() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuDefaultZ { z: 0.75 })?.default_z,
            0.75,
        );
        Ok(())
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn depth_offset_scale() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuDepthOffsetScale { scale: 0.25 })?.depth_offset_scale,
            0.25,
        );
        Ok(())
    }

    #[test]
    fn load_indirect_state() -> Result<()> {
        exec_single_command(GpuLoadIndirectState {}).map(|_| ())
    }

    #[test]
    fn pixel_shader_constants_simple() -> Result<()> {
        let c = (1.0, 2.0, 3.0, 4.0);
        let state = exec_single_command(GpuPixelShaderConstants {
            regmask: 1 << 1,
            consts: vec![c],
        })?;
        assert_eq!(state.consts[1], c);
        Ok(())
    }

    #[test]
    fn pixel_shader_constants_sparse() -> Result<()> {
        let c1 = (1.0, 2.0, 3.0, 4.0);
        let c3 = (5.0, 6.0, 7.0, 8.0);
        let c4 = (9.0, 10.0, 11.0, 12.0);
        let state = exec_single_command(GpuPixelShaderConstants {
            regmask: 1 << 1 | 1 << 3 | 1 << 4,
            consts: vec![c1, c3, c4],
        })?;
        assert_eq!(state.consts[1], c1);
        assert_eq!(state.consts[3], c3);
        assert_eq!(state.consts[4], c4);
        Ok(())
    }

    #[test]
    fn pixel_shader_constants_incremental() -> Result<()> {
        let c0 = (1.0, 2.0, 3.0, 4.0);
        let c1 = (5.0, 6.0, 7.0, 8.0);
        let mut build = ExecBuilder::new();
        build.batch_command(&GpuPixelShaderConstants {
            regmask: 1 << 0,
            consts: vec![c0],
        });
        build.batch_command(&GpuPixelShaderConstants {
            regmask: 1 << 1,
            consts: vec![c1],
        });
        let state = Device::new(Devid::I945GM)?.exec(build.finalize())?;
        assert_eq!(state.consts[0], c0);
        assert_eq!(state.consts[1], c1);
        Ok(())
    }

    #[test]
    fn rast_rules_texkill() -> Result<()> {
        // texkill 4d checks
        assert_eq!(
            exec_single_command(GpuRasterizationRules {
                texkill_4d_enable: true,
                texkill_4d_modify_enable: true,
                ..Default::default()
            })?
            .texkill_4d,
            true,
        );
        assert_eq!(
            exec_single_command(GpuRasterizationRules {
                texkill_4d_enable: false,
                texkill_4d_modify_enable: true,
                ..Default::default()
            })?
            .texkill_4d,
            false,
        );
        Ok(())
    }

    #[test]
    fn rast_rules_point_rast() -> Result<()> {
        for point_rast_rule in [
            GpuPointRastRule::UpperLeft,
            GpuPointRastRule::UpperRight,
            GpuPointRastRule::LowerRight,
        ]
        .iter()
        {
            assert_eq!(
                exec_single_command(GpuRasterizationRules {
                    point_rast_rule: *point_rast_rule,
                    point_rast_rule_modify_enable: true,
                    ..Default::default()
                })?
                .point_rast_rule,
                *point_rast_rule,
            );
        }
        Ok(())
    }

    #[test]
    fn rast_rules_pv() -> Result<()> {
        for pv in [GpuLineProvokingVertex::V0, GpuLineProvokingVertex::V1].iter() {
            assert_eq!(
                exec_single_command(GpuRasterizationRules {
                    line_provoking_vertex: *pv,
                    line_provoking_vertex_modify_enable: true,
                    ..Default::default()
                })?
                .line_provoking_vertex,
                *pv,
            );
        }

        for pv in [
            GpuTriFanProvokingVertex::V0,
            GpuTriFanProvokingVertex::V1,
            GpuTriFanProvokingVertex::V2,
        ]
        .iter()
        {
            assert_eq!(
                exec_single_command(GpuRasterizationRules {
                    tri_fan_provoking_vertex: *pv,
                    tri_fan_provoking_vertex_modify_enable: true,
                    ..Default::default()
                })?
                .tri_fan_provoking_vertex,
                *pv,
            );
        }

        Ok(())
    }

    #[test]
    fn scissor_enable() -> Result<()> {
        assert_eq!(
            exec_single_command(GpuScissorEnable {
                modify_enable: true,
                enable: false
            })?
            .scissor_enable,
            false
        );
        assert_eq!(
            exec_single_command(GpuScissorEnable {
                modify_enable: true,
                enable: true
            })?
            .scissor_enable,
            true
        );

        // Emit a nonsense scissor enable command and make the emulator throws
        // an error.  No driver should do this, even though it's a valid packet.
        assert!(exec_single_command(GpuScissorEnable {
            modify_enable: false,
            enable: true
        })
        .is_err());
        Ok(())
    }
    #[test]
    fn scissor_rect() -> Result<()> {
        let rect = GpuScissorRectangle {
            xmin: 1,
            ymin: 2,
            xmax: 3,
            ymax: 0xffff,
        };
        assert_eq!(exec_single_command(rect)?.scissor, rect);
        Ok(())
    }

    #[test]
    fn minimal_render() -> Result<()> {
        let mut build = ExecBuilder::new();

        let width = 5;
        let cpp = 4;
        let pitch = ((width + 3) * cpp) & !3;
        let height = 9;

        let color_buf_id = build.new_buf(pitch * height);

        build.batch_command(&MiFlush {
            end_scene: false,
            scene_count: false,
            render_cache_flush_inhibit: false,
            map_cache_invalidate: true,
        });

        let color_info = GpuBufferInfo {
            aux_buffer_id: false,
            buffer_id: GpuBufferID::Color,
            use_fence: false,
            tiled: false,
            tiled_y: false,
            pitch,
            offset: build.exec.memory.bufs[color_buf_id].offset,
        };

        build.batch_command(&color_info);

        build.batch_command(&GpuDrawingRectangle {
            clip_xmin: 0,
            clip_ymin: 0,
            clip_xmax: width as u16,
            clip_ymax: height as u16,
            draw_x: 0,
            draw_y: 0,
        });

        build.batch_command(&GpuPrimitive {
            vertex_indirect: false,
            prim_type: GpuPrimType::TriFan,
            access_random: false,
            len: 4,
        });
        build.batch_f(-1.0);
        build.batch_f(-1.0);

        build.batch_f(1.0);
        build.batch_f(-1.0);

        build.batch_f(1.0);
        build.batch_f(1.0);

        build.batch_f(-1.0);
        build.batch_f(1.0);

        build.batch_command(&MiFlush {
            end_scene: false,
            scene_count: false,
            render_cache_flush_inhibit: false,
            map_cache_invalidate: true,
        });

        let exec = build.finalize();

        let state = Device::new(Devid::I945GM)?.exec(exec)?;
        let cb = &state.memory.bufs[color_buf_id].data;
        for y in 0..height {
            for x in 0..width {
                let offset = y * pitch + x * cpp;
                assert_eq!(cb.read_dword(offset)?, 0x00ff0000);
            }
        }

        Ok(())
    }
}
