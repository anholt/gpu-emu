//! This is the C library for accessing the I915 GPU emulator.

use super::*;
use num_traits::FromPrimitive;
use std::os::raw::{c_uint, c_ushort};
use std::ptr::null_mut;

#[repr(C)]
pub struct gpu_emu_i915_buf {
    pub data: *mut u8,
    pub offset: u32,
    pub size: u32,
}

/// Creates an instance of the i915 device to be owned by the C caller.
/// Must be manually freed with gpu_emu_i915_device_free().
#[no_mangle]
pub unsafe extern "C" fn gpu_emu_i915_device_new(devid: c_ushort) -> *mut Device {
    let _ = env_logger::builder().try_init();

    let devid = match Devid::from_u16(devid) {
        Some(devid) => devid,
        None => {
            error!("Unknown devid 0x{:x}", devid);
            return null_mut();
        }
    };

    let device = match Device::new(devid) {
        Ok(device) => device,
        Err(e) => {
            error!("Failed to create device: {:?}", e);
            return null_mut();
        }
    };

    Box::into_raw(Box::new(device))
}

/// Frees a device allocated with gpu_emu_i915_device_new().
#[no_mangle]
pub unsafe extern "C" fn gpu_emu_i915_device_free(device: *mut Device) {
    if device.is_null() {
        return;
    }

    Box::from_raw(device);
}

/// Runs a set of batch commands on the given I915 device.
///
/// # Arguments
///
/// * `device` Exclusive pointer to the device to emulate on.
/// * `bufs` Pointer to an array of struct gpu_emu_i915_buf describing the memory regions available.
/// * `buf_count` Mumber of elements in the `bufs` array.
/// * `batch_offset` byte offset within the first buf in the array for the start of the batchbuffer.
/// * `batch_len` byte length starting from `batch_offset` to parse and emulate.
#[no_mangle]
pub unsafe extern "C" fn gpu_emu_i915_exec(
    device: *mut Device,
    bufs: *mut gpu_emu_i915_buf,
    buf_count: c_uint,
    batch_offset: c_uint,
    batch_len: c_uint,
) -> bool {
    if device.is_null() {
        return false;
    }

    let device = &mut *device;

    let mut memory = Memory { bufs: Vec::new() };
    let bufs = core::slice::from_raw_parts_mut(bufs, buf_count as usize);
    for buf in bufs {
        let data = core::slice::from_raw_parts_mut(buf.data, buf.size as usize);
        memory.bufs.push(ExecBuf {
            data: DeviceBuf(data.to_vec()),
            offset: buf.offset,
        });
    }

    let exec = Exec {
        batch_index: 0,
        batch_offset,
        batch_len,
        memory: memory,
    };

    match device.exec(exec) {
        Ok(_) => true,
        _ => false,
    }
}
