This repository is for a collection of GPU emulation tools.

Currently I'm working on a GL-based i915 emulator so that we can keep i915/i915g
from regressing in CI.  Install its C library with:

cargo install cargo-c
cd gpu/i915
cargo cinstall --manifest-path gpu/i915/Cargo.toml

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the
Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
